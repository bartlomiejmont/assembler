#include <stdio.h>

int main (){

    char x[] = "hello world";
    char y;

    asm volatile (
        ".intel_syntax noprefix;"
        "mov eax, [%1];"
        "mov ebx,0;"
        

        "mov [%0],eax;"
        ".att_syntax prefix;"
        :"=r" (y)  //wyjscie
        :"r" (x)  //wejscie
        :"eax","ebx"  //skutki uboczne
    );

// for(int i = sizeof(y)-2; i>=0; i--){
    printf("x=%c\n", y);
// }
    

    return 0;
}
