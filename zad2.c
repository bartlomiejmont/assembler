#include <stdio.h>

int main (){

    int x = 0xFF0F;
    int y = 0;

    asm volatile (
        ".intel_syntax noprefix;"
        "mov eax, %1;"
        "mov ebx,0;"
        "mov ecx,32;"

        "petla:" // etykieta 32 krokowa petla 
        "shr eax;" //przesuniecie w prawo
        "jnc ndodaj;" //sprawdzanie czy zero
        "inc ebx;" // inkrementacja
        "ndodaj:"
        "dec ecx;" // dekrementacja
        "jnz petla;" // goto petla jesli eax nie ma przeniesienia
        "mov %0,ebx;"

        ".att_syntax prefix;"
        :"=r" (y)  //wyjscie
        :"r" (x)  //wejscie
        :"eax","ebx","ecx"  //skutki uboczne
    );

    printf("x=%d y=%d\n", x,y);

    return 0;
}
