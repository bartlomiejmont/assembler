#include <stdio.h>

int main (){

    int x = 0xFFFFFFFF;
    int y = 0;

    asm volatile (
        ".intel_syntax noprefix;"
        "mov eax, %1;"
        "mov ebx,0;"
        "mov ecx,32;"
        "mov edx, 0;"

        "petla:" // etykieta 32 krokowa petla 
        "shr eax;" //przesuniecie w prawo
        "jnc ndodaj;" //sprawdzanie czy mozna przesunac
        "inc ebx;" // inkrementacja
        "ndodaj:" 
        "jc nzeruj;" // sprawdzanie czy przy przesunieciu 1
        "mov ebx,0;" // jesli nie to zeruj ebx
        "nzeruj:"
        "cmp ebx, edx;" // sprawdzenie co jest wieksze
        "jb nzamieniaj;" // sprawdza czy ebx mniejsze od edx
        "mov edx, ebx;" // jesli nie to zamien edx na ebx
        "nzamieniaj:" 
        "dec ecx;" // dekrementacja
        "jnz petla;" // goto petla jesli ecx != 0
        "mov %0,edx;" 

        ".att_syntax prefix;"
        :"=r" (y)  //wyjscie
        :"r" (x)  //wejscie
        :"eax","ebx","ecx","edx"  //skutki uboczne
    );

    printf("x=%d y=%d\n", x,y);

    return 0;
}
