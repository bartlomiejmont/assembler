#include <stdio.h>

int main (){

    int x = 0xFFF0;
    int y = 0;

    asm volatile (
        ".intel_syntax noprefix;"
        "mov eax, %1;"
        "mov ebx,0;"

        "sub eax, 0;"
        "jz koniec;" // goto koniec jesli eax == 0
        "petla:" // etykieta
        "inc ebx;" // inkrementacja
        "shr eax;" // przesuniecie bitowe w prawo
        "jnc petla;" // goto petla jesli eax nie ma przeniesienia
        "koniec:"  // etykieta
        "mov %0,ebx;"

        ".att_syntax prefix;"
        :"=r" (y)  //wyjscie
        :"r" (x)  //wejscie
        :"eax","ebx"  //skutki uboczne
    );

    printf("x=%d y=%d\n", x,y);

    return 0;
}